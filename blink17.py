from gpiozero import LED
from time import sleep

led = LED(17)

for num in range (0,20):
    led.on()
    sleep(1/(num+1))
    led.off()
    sleep(1/(num+1))

