from gpiozero import LED,Button, PWMOutputDevice
from time import sleep
from random import randint

button0 = Button(17)
button1 = Button(5)
button2 = Button(19)
buzzer = PWMOutputDevice
pScore = 0
cScore = 0
rLeds = [LED(4), LED(22), LED(13)]
gLeds = [LED(27), LED(6), LED(26)]

def animateWinner (leds, blinks):
    for i in range(blinks):
        for light in leds:
            light.on()
        sleep(.25)
        for light in leds:
            light.off()
        sleep(.25)

def result(pPick,win,lose) :
    global pScore, cScore
    cPick = randint(0,2)
    gLeds[pPick].on()
    rLeds[cPick].on()
    sleep(2)
    if cPick == win :
        print("You win!")
        pScore += 1
        animateWinner(gLeds, 5)
    elif cPick == lose:
            print ("You lose!")
            cScore +=1
            animateWinner(rLeds, 5)
    else:
        print("It's a tie!")
        animateWinner(gLeds+rLeds, 3)
    rLeds[cPick].off()
    gLeds[pPick].off()

def rock():
    pPIck = 0
    result(0,2,1)
    
def paper():
    pPick = 1
    result(1,0,2)
    
def scissors():
    pPick = 2
    result(2,1, 0)

def cleanup():
    for i in range (0,2):
        gLeds[i].off()
        rLeds[i].off()

button0.when_pressed = rock
button1.when_pressed = paper
button2.when_pressed = scissors

try:
    while pScore < 3 and cScore < 3:
        sleep(.25)
except:
    print("Program Terminated")
    cleanup()
    exit()
    
print(pScore, cScore)


        
            
