from gpiozero import LED, Button, PWMOutputDevice
from time import sleep

led0 = LED(17)
led1 = LED(27)
led2 = LED(22)
led3 = LED(5)
led4 = LED(6)
button0 = Button(13)
buzzer = PWMOutputDevice(18)

leds = [led0, led1, led2, led3, led4]

ledNum = 0
hit = False

def attempt():
    global hit
    hit = True
    leds[ledNum].off()
    buzzer.value = .5
    sleep(.4)
    buzzer.value = 0
button0.when_released = attempt

while True:
    leds[ledNum].on()
    sleep(.4)
    leds[ledNum].off()
    if hit == True:
        if ledNum == 2:
            print ("hit!")
            hit = False
        else:
            print("miss")
            hit = False
    ledNum +=1
    if ledNum ==5:
        ledNum = 0
