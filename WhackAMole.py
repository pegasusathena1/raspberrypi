from gpiozero import LED, Button, PWMOutputDevice
from time import sleep
from random import randint

led0 = LED(4)
led1 = LED(22)
led2 = LED(13)
button0 = Button(17)
button1 = Button(5)
button2 = Button(19)
buzzer = PWMOutputDevice(25)
score = 0
leds = [led0, led1, led2]

def success():
    global score
    leds[ledNum].off()
    score +=1
    buzzer.value = .5
    sleep(.1)
    buzzer.value = 0

def whack0():
    if ledNum ==0:
        success()
def whack1():
    if ledNum ==1:
        success()
def whack2():
    if ledNum ==2:
        success()

button0.when_pressed = whack0
button1.when_pressed = whack1
button2.when_pressed = whack2

for num in range (0,20):
    ledNum=randint(0,2)
    leds[ledNum].on()
    sleep(.5)
    leds[ledNum].off()
    sleep(.05)

print(score)


                       
