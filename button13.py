from gpiozero import LED, Button

led1 = LED(17)
led2 = LED(21)
button1 = Button(13)
button2 = Button(26)

button1.when_released = led1.toggle
button2.when_released = led2.toggle
