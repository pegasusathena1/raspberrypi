from gpiozero import LED, Button
from time import sleep

led0 = LED(17)
led1 = LED(27)
led2 = LED(22)
led3 = LED(5)
led4 = LED(6)
button0 = Button(13)

leds = [led0, led1, led2, led3, led4]

ledNum = 0
def switchLight() :
    global ledNum
    leds[ledNum].off()
    ledNum = ledNum + 1
    if ledNum == 5:
        ledNum = 0
    leds[ledNum].on()

led0.on()

button0.when_released = switchLight

    
