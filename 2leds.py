from    gpiozero import LED, Button

led0 = LED(17)
led1 = LED(22)
button0 = Button(13)

led0.toggle()

def lightAll():
    led0.toggle()
    led1.toggle()

button0.when_released = lightAll
